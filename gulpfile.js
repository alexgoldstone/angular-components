var gulp = require('gulp');
var path = require('path');
var util = require('gulp-util');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var addStream = require('add-stream');
var sourcemaps = require('gulp-sourcemaps');
var gulpNgConfig = require('gulp-ng-config');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var templateCache = require('gulp-angular-templatecache');
var browserSync = require('browser-sync').create();

var config = {};

config.path = {
    public: path.join(__dirname, './public/'),

    scss: path.join(__dirname, './assets/scss/**/*.scss'),
    susy: path.join(__dirname, './node_modules/susy/sass/'),

    angular: path.join(__dirname, './node_modules/angular/angular.min.js'),
    app: path.join(__dirname, './app/app.js'),
    components: path.join(__dirname, './app/components/**/*.js'),
    services: path.join(__dirname, './app/services/**/*.js'),
    templates: path.join(__dirname, './app/components/**/*.html'),
    js: path.join(__dirname, './assets/js/**/*.js'),

    assets: path.join(__dirname, './assets/public/**/*')
};

/**
 * Command line arguments to specify the environment
 *
 * --dev
 * --production (default)
 */
config.env = {
    dev: !!util.env.production ? false : (!!util.env.dev ? true : false),
    production: !!util.env.production || (!!util.env.dev ? false : true)
};

gulp.task('sass', function () {
    var stream = gulp.src(config.path.scss)
        .pipe(config.env.dev ? sourcemaps.init() : util.noop())
        .pipe(sass({
            includePaths: [
                config.path.susy
            ],
            outputStyle: config.env.dev ? 'expanded' : 'compressed'
        }).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(config.env.dev ? sourcemaps.write() : util.noop())
        .pipe(gulp.dest(config.path.public))
        .pipe(browserSync.stream());

    notify('Stylesheets Compiled!');

    return stream;
});

function ngConfig() {
    return gulp.src(path.join(__dirname, './env.json'))
        .pipe(gulpNgConfig('app.config', {
            environment: config.env.dev ? 'dev' : 'production'
        }));
}

gulp.task('js', function() {
    var stream = gulp.src([
            config.path.angular,
            config.path.app,
            config.path.components,
            config.path.services,
            config.path.js
        ])
        .pipe(addStream.obj(ngConfig()))
        .pipe(config.env.dev ? sourcemaps.init() : util.noop())
        .pipe(concat('app.js'))
        .pipe(uglify().on('error', error))
        .pipe(config.env.dev ? sourcemaps.write() : util.noop())
        .pipe(gulp.dest(config.path.public))
        .pipe(browserSync.stream());

    notify('JavaScript Compiled!');

    return stream;
});

gulp.task('assets', function() {
    var stream = gulp.src([
            config.path.assets
        ])
        .pipe(gulp.dest(config.path.public));

    if(config.env.dev)
        browserSync.reload();

    notify('Assets Copied!');

    return stream;
});

gulp.task('templates', function () {
    var stream = gulp.src(config.path.templates)
        .pipe(templateCache('templates.js', {
            module: 'app',
            root: '',
            standalone: false
        }))
        .pipe(gulp.dest(config.path.public))
        .pipe(browserSync.stream());

    notify('Template Cache Compiled!');

    return stream;
});

gulp.task('browser-sync', function() {
    if(config.env.dev) {
        browserSync.init({
            server: {
                baseDir: config.path.public
            }
        });
    }
});

gulp.task('watch', ['sass', 'js', 'templates', 'browser-sync'], function () {
    if(config.env.dev) {
        gulp.watch(config.path.scss, ['sass']);
        gulp.watch([
            config.path.angular,
            config.path.app,
            config.path.components,
            config.path.services,
            config.path.js
        ], ['js']);
        gulp.watch(config.path.templates, ['templates']);
        gulp.watch(config.path.assets, ['assets']);
        notify('Watching...');
    }
});

gulp.task('default', ['assets', 'watch']);

/**
 * Log a message or series of messages using chalk's blue color.
 * Can pass in a string, object or array.
 */
function log(msg, color) {
    if(typeof color === 'undefined')
        color = util.colors.blue;

    if (typeof(msg) === 'object') {
        for (var item in msg) {
            if (msg.hasOwnProperty(item)) {
                util.log(color(msg[item]));
            }
        }
    } else {
        util.log(color(msg));
    }
}

function error(msg) {
    log(msg, util.colors.red);
    this.emit('end');
}

/**
 * Show OS level notification using node-notifier
 * Pass string 'message' or object {title, subtitle, message, etc.}
 */
function notify(options) {
    var notifier = require('node-notifier');
    var notifyOptions = {
        title: 'Gulp',
        sound: 'Pop'
    };

    if(typeof options === 'string')
        notifyOptions.message = options;
    else if(typeof options === 'object')
        Object.assign(notifyOptions, options);

    if(typeof notifyOptions.message !== 'undefined')
        log(notifyOptions.message);

    // Production servers don't support notifications
    if(!config.env.production)
        notifier.notify(notifyOptions);
}
