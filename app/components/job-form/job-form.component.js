(function() {
    'use strict';

    angular.module('app').component('jobForm', {
        bindings: {
            serviceName: '@service'
        },
        controller: 'JobFormController',
        templateUrl: 'job-form/job-form.html'
    });
})();
