(function() {
    'use strict';

    angular.module('app').controller('JobFormController', JobFormController);

    JobFormController.$inject = ['$scope', '$injector'];

    function JobFormController($scope, $injector) {
        var $ctrl = this;

        $ctrl.selected = null;
        $ctrl.job = {};

        $ctrl.dataService = $injector.get($ctrl.serviceName);

        $ctrl.create = function(model) {
            $ctrl.dataService.createJob(angular.copy(model));
        };

        $ctrl.update = function(model) {
            $ctrl.dataService.updateJob(model);
        };

        $ctrl.deselect = function(model) {
            $ctrl.dataService.deselectJob();
        };

        $ctrl.dataService.on('select', function(job) {
            $ctrl.selected = job;
            $ctrl.job = angular.copy(job);
        });

        $ctrl.dataService.on('deselect', function(job) {
            $ctrl.selected = null;
            $ctrl.job = {};
        });
    }
})();
