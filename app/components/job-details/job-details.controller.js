(function() {
    'use strict';

    angular.module('app').controller('JobDetailsController', JobDetailsController);

    JobDetailsController.$inject = ['$scope', '$injector'];

    function JobDetailsController($scope, $injector) {
        var $ctrl = this;

        $ctrl.job = null;

        $ctrl.dataService = $injector.get($ctrl.serviceName);

        $ctrl.dataService.on('select', function(job) {
            $ctrl.job = job;
        });

        $ctrl.dataService.on('deselect', function(job) {
            $ctrl.job = null;
        });
    }
})();
