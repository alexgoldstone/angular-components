(function() {
    'use strict';

    angular.module('app').component('jobDetails', {
        bindings: {
            serviceName: '@service'
        },
        controller: 'JobDetailsController',
        templateUrl: 'job-details/job-details.html'
    });
})();
