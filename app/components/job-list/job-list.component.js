(function() {
    'use strict';

    angular.module('app').component('jobList', {
        bindings: {
            serviceName: '@service'
        },
        controller: 'JobListController',
        templateUrl: 'job-list/job-list.html'
    });
})();
