(function() {
    'use strict';

    angular.module('app').controller('JobListController', JobListController);

    JobListController.$inject = ['$scope', '$injector'];

    function JobListController($scope, $injector) {
        var $ctrl = this;

        $ctrl.jobs = [];

        $ctrl.selected = null;

        $ctrl.dataService = $injector.get($ctrl.serviceName);

        $ctrl.onClick = function(model) {
            if(model == $ctrl.selected) {
                $ctrl.dataService.deselectJob();
            }
            else {
                $ctrl.dataService.selectJob(model);
            }
        };

        $ctrl.dataService.on('select', function(model) {
            $ctrl.selected = model;
        });

        $ctrl.dataService.on('deselect', function(model) {
            $ctrl.selected = null;
        });

        $ctrl.dataService.on('create', function(model) {
            $ctrl.jobs.push(model);
            $ctrl.dataService.selectJob(model);
        });

        $ctrl.dataService.getAll()
        .then(function(response) {
            $ctrl.jobs = response;
        })
        .catch(function(response) {
            console.log(response);
        });
/*
        $ctrl.dataService.getSingle('test')
        .then(function(response) {
            console.log(response);
        })
        .catch(function(response) {
            console.log(response);
        });
*/
    }
})();
