(function() {
    'use strict';

    angular.module('app').service('ApiService', ApiService);

    ApiService.$inject = ['$http', '$q', 'EnvironmentConfig'];

    function ApiService($http, $q, EnvironmentConfig) {
        return {
            endpoint: '/',
            getAll: function (params, cache) {
                return makeRequest('GET', EnvironmentConfig.api + this.endpoint, params, null, cache);
            },
            getSingle: function (id, params, cache) {
                return makeRequest('GET', EnvironmentConfig.api + this.endpoint + '/' + id, params, null, cache);
            },
            events: {},
            on: function(event, handler) {
                if(this.events[event] === undefined) {
                    this.events[event] = [];
                }

                this.events[event].push(handler);
            },
            trigger: function (event, data) {
                if(this.events[event] !== undefined) {
                    for(var i = 0; i < this.events[event].length; i++) {
                        this.events[event][i](data);
                    }
                }
            }
        };

        function makeRequest(method, url, params, data, cache) {
            cache = cache !== false;
            var deferred = $q.defer();

            $http({
                method: method,
                url: url,
                params: params,
                data: data,
                cache: cache,
            })
            .then(function makeRequestSuccess(response) {
                deferred.resolve(response.data);
            }, function makeRequestFailed(response) {
                deferred.reject(response.data);
            });

            return deferred.promise;
        }
    }
})();
