(function() {
    'use strict';

    angular.module('app').service('JobService', JobService);

    JobService.$inject = ['ApiService'];

    function JobService(ApiService) {
        var service = Object.create(ApiService);
        service.endpoint = '/jobs.json';

        service.selected = null;

        service.selectJob = function(job) {
            service.selected = job;
            service.trigger('select', service.selected);
        };

        service.deselectJob = function() {
            var job = service.selected;
            service.selected = null;
            service.trigger('deselect', job);
        };

        service.createJob = function(job) {
            service.trigger('create', job);
        };

        service.updateJob = function(job) {
            if(service.selected) {
                angular.copy(job, service.selected);
                service.trigger('update', service.selected);
            }
        };

        return service;
    }
})();
